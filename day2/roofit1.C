using namespace RooFit;
 
void roofit1 () {
 
  TH1F *hist = new TH1F("hist", "hist", 20, -10, 10);
  for(int ii = 0; ii < 100; ii++) {
    hist->Fill(gRandom->Gaus(0, 1));
  }
 
  RooRealVar x("x", "x", -10, 10);
  RooRealVar mean("mean", "Mean of Gaussian", 0, -10, 10);
  RooRealVar sigma("sigma", "Width of Gaussian", 1, -10, 10);
 
  RooGaussian gauss("gauss", "gauss(x, mean, sigma)", x, mean, sigma);
 
  RooDataHist data("data", "binned dataset", x, hist);
 
  RooPlot* xframe = x.frame();
  data.plotOn(xframe);
 
  gauss.fitTo(data);
  gauss.plotOn(xframe);
  xframe->Draw();
}
