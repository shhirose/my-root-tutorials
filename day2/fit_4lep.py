#!/usr/bin/env python3

import ROOT
import copy

bin_scale = 4

datasets = {'data': [0],
            'zz': [363356, 363490],
            'others': [361107, 361106, 361108, 363358, 363359, 363360, 363489, 363491, 363492, 363493, 410000, 410011, 410012],
            'higgs': [344235, 345060]}

colors = [0, ROOT.kAzure+7, ROOT.kGreen+2, ROOT.kRed]

f = ROOT.TFile('histograms.root')

histograms = {}

for icat, category in enumerate(datasets.keys()):
    for dsid in datasets[category]:
        h = copy.copy(f.Get('h' + str(dsid)))
        h.Rebin(bin_scale)
        
        if not category in histograms.keys():
            histograms[category] = h
        else:
            histograms[category].Add(h)

    if not category == 'data':
        histograms[category].SetFillColor(colors[icat])
        histograms[category].SetLineColor(colors[icat])

c1 = ROOT.TCanvas('c1', 'c1')

m = ROOT.RooRealVar('m', 'mass', 0, 300)

# Prepare data histogram

hroo_data   = ROOT.RooDataHist('hroo_data',   '', m, histograms['data'])
hroo_sig    = ROOT.RooDataHist('hroo_sig',    '', m, histograms['higgs'])
hroo_bkg_zz = ROOT.RooDataHist('hroo_bkg_zz', '', m, histograms['zz'])
hroo_bkg    = ROOT.RooDataHist('hroo_bkg',    '', m, histograms['others'])

# Prepare histogram PDFs

pdf_sig    = ROOT.RooHistPdf('pdf_sig',    '', m, hroo_sig,    0)
pdf_bkg_zz = ROOT.RooHistPdf('pdf_bkg_zz', '', m, hroo_bkg_zz, 0)
pdf_bkg    = ROOT.RooHistPdf('pdf_bkg',    '', m, hroo_bkg,    0)

# Set event yields

n_sig    = ROOT.RooRealVar('n_sig',    'nSig', -100, 1000)
n_bkg_zz = ROOT.RooRealVar('n_bkg_zz', 'nbkg', 0, 1000)
n_bkg    = ROOT.RooRealVar('n_bkg',    'nbkg', 0, 1000)

pdf_tot = ROOT.RooAddPdf('model', '', ROOT.RooArgList(pdf_sig, pdf_bkg_zz, pdf_bkg), ROOT.RooArgList(n_sig, n_bkg_zz, n_bkg))

# Fix n_bkg

n_bkg.setVal(histograms['others'].Integral())
n_bkg.setConstant(True)

# Perform a fit

results = pdf_tot.fitTo(hroo_data, ROOT.RooFit.Save(), ROOT.RooFit.Minos(True))
nll = results.minNll()

n_sig_expected = histograms['higgs'].Integral()
n_bkg_zz_expected = histograms['zz'].Integral()

# Make a stacked histogram

histograms['zz'].Scale( n_bkg_zz.getVal() / histograms['zz'].Integral() )
histograms['higgs'].Scale( n_sig.getVal() / histograms['higgs'].Integral() )

hstack = ROOT.THStack('hs', 'hs')
hstack.Add(histograms['others'])
hstack.Add(histograms['zz'])
hstack.Add(histograms['higgs'])

h_tot = copy.copy(histograms['others'])
h_tot.Add(histograms['zz'])
h_tot.Add(histograms['higgs'])

# Get maximum of the pad

maxVal = 0
for i in range(histograms['data'].GetNbinsX()):
    val = histograms['data'].GetBinContent(i+1) + histograms['data'].GetBinError(i+1)
    if val > maxVal:
        maxVal = val
    
# Draw plots

frame = ROOT.gPad.DrawFrame(0, 0, 300, maxVal * 1.05)

hstack.Draw('hist same')

h_tot.SetFillStyle(3154)
h_tot.SetFillColor(ROOT.kGray)
h_tot.Draw('e2 same')

roo_frame = m.frame(ROOT.RooFit.Title('test'))
hroo_data.plotOn(roo_frame)
roo_frame.Draw('same')

ROOT.gPad.RedrawAxis()

mu = n_sig.getVal() / n_sig_expected
err_l = n_sig.getErrorLo() / n_sig_expected
err_h = n_sig.getErrorHi() / n_sig_expected

print("******* Summary of results *******")
print("N(sig) = " + str(n_sig.getVal()))
print("mu(Higgs) = " + str(mu) + " +" + str(err_h) + " -" + str(abs(err_l)))
print("Minimum negative Log-likelihood = " + str(nll))

input()
