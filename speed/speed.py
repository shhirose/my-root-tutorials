#!/usr/bin/env python3

import ROOT

hist = ROOT.TH1F("hist", "", 100, 0, 20)

for i in range(100000000):
    hist.Fill(ROOT.gRandom.Gaus(10, 5))

c1 = ROOT.TCanvas("c1", "c1")
hist.Draw()

