#include <TH1.h>
#include <TCanvas.h>
#include <TRandom.h>

int main() {

  int event = 0;
  int hitFlag = 0;
  float actualX = 0;
  float trackX = 0;
  float pt = 0;
  float d0 = 0;
  float chi2 = 0;
  float dof = 0;

  TH1F *hist = new TH1F("hist", "", 100, 0, 20);

  for (int i=0; i<1000000000; i++) {
    hist->Fill(gRandom->Gaus(10, 5));
  }

  TCanvas *c1 = new TCanvas("c1", "c1");
  hist->Draw();

  return 0;
}
