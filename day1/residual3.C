void residual3() {

  int event = 0;
  int hitFlag = 0;
  float actualX = 0;
  float trackX = 0;
  float pt = 0;
  float d0 = 0;
  float chi2 = 0;
  float dof = 0;

  TH1F *hist1 = new TH1F("hist1", ";Residual [mm]", 100, -0.5, 0.5);
  TH1F *hist2 = new TH1F("hist2", ";Residual [mm]", 100, -0.5, 0.5);
  
  std::ifstream fin("../data/track_data.dat");
  while (fin >> event >> hitFlag >> actualX >> trackX >> pt >> d0 >> chi2 >> dof) {
    if (hitFlag == 1) {
      if (chi2/dof < 2.5) hist1->Fill(actualX - trackX);
      else                hist2->Fill(actualX - trackX);
    }
  }

  TCanvas *c1 = new TCanvas("c1", "c1");
  hist1->SetLineColor(1);
  hist2->SetLineColor(2);
  
  hist1->DrawNormalized();
  hist2->DrawNormalized("same");

}
